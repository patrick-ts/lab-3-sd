# Getting Started with Jeify

Você foi designado para desenvolver uma API REST para uma plataforma de streaming de música. A empresa está crescendo rapidamente e requer uma solução robusta para gerenciar sua biblioteca de músicas de maneira eficiente.

## Project Overview

- This project has a docker-compose that up a PostgreSQL container.
- In jala.sd.jeify.cmd, there is a script that seeds the database if it has no data when the project runs.
- Bellow you can find all end points for this project.

## Features

---

### Musics

- It should be able to **add** musics in the database;
- It should be able to **delete** a music by id;
- It should be able to **update** some music detail by id;
- It should be able to **list** all musics;
- It should be able to **find** a music by id;
- It should be able to **filter** musics by title, artist and genre.

## API Reference

---

### Body

```json
{
  "id": "<uuid>",
  "title": "<string>",
  "author": "<string>",
  "genre": "<string>",
  "duration": "<int>"
}
```

### Endpoints

#### Create a New Music

```http request
POST /api/musics
```

#### Retrieve All Musics

```http request
GET /api/musics
```

#### Update a Music

```http request
PUT /api/musics/{id}
```

#### Delete a Music

```http request
DELETE /api/musics/{id}
```

#### Retrieve Musics by Label

```http request
GET /api/musics/filter?artist=d4vd&genre=Rock+Indie
```

## Structure

- http:  and data validation
    - controllers: HTTP requests
    - dtos: Data transfer objects
- service: Business logic
- domain: Core
    - entities: Entities / Models
    - repository: Database queries representation
