package jala.sd.jeify;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JeifyApplication {
	public static void main(String[] args) {
		SpringApplication.run(JeifyApplication.class, args);
	}
}
