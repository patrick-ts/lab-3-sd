package jala.sd.jeify.domain.repositories;

import jala.sd.jeify.domain.entities.Music;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.util.List;
import java.util.UUID;

@Repository
public interface MusicRepository extends JpaRepository<Music, UUID> {
    @Query("SELECT m FROM Music m WHERE m.genre LIKE :genre%")
    List<Music> filterByGenre(@Param("genre") String genre);

    @Query("SELECT m FROM Music m WHERE m.artist LIKE :artist%")
    List<Music> filterByArtist(@Param("artist") String artist);

    @Query("SELECT m FROM Music m WHERE m.title LIKE :title%")
    List<Music> filterByTitle(@Param("title") String title);
}
