package jala.sd.jeify.http.controllers;

import io.micrometer.common.util.StringUtils;
import jakarta.validation.Valid;
import jala.sd.jeify.domain.entities.Music;
import jala.sd.jeify.http.dtos.MusicRequestDTO;
import jala.sd.jeify.services.MusicService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@Controller
@RestController
@RequestMapping("/api/musics")
public class MusicController {
    private final MusicService musicService;

    @Autowired
    public MusicController(MusicService musicService) {
        this.musicService = musicService;
    }

    @GetMapping
    public ResponseEntity<List<Music>> getAll() {
        return ResponseEntity.status(HttpStatus.OK).body(musicService.getAll());
    }

    @PostMapping
    public ResponseEntity<Music> save(@RequestBody @Valid MusicRequestDTO data) {
        Music music = new Music();
        BeanUtils.copyProperties(data, music);
        return ResponseEntity.status(HttpStatus.OK).body(musicService.save(music));
    }

    @PutMapping("/{music_id}")
    public ResponseEntity<Object> updateMusic(@PathVariable UUID music_id, @RequestBody @Valid MusicRequestDTO musicRequestDTO) {
        Music music = musicService.findById(music_id);

        BeanUtils.copyProperties(musicRequestDTO, music);

        return ResponseEntity.status(HttpStatus.OK).body(musicService.update(music));
    }

    @DeleteMapping("/{music_id}")
    public ResponseEntity<Object> deleteMusic(@PathVariable UUID music_id) {
        musicService.delete(music_id);
        return ResponseEntity.status(HttpStatus.OK).body("Music " + music_id + " has been deleted.");
    }

    @GetMapping(value = "/filter")
    public ResponseEntity<Object> getMusicsByQueryParameter(
            @RequestParam(name = "artist", required = false) String artist,
            @RequestParam(name = "genre", required = false) String genre,
            @RequestParam(name = "title", required = false) String title) {
        Set<Music> musics = new HashSet<>();

        if (StringUtils.isNotEmpty(artist)) musics.addAll(musicService.filterByArtist(artist));
        if (StringUtils.isNotEmpty(genre)) musics.addAll(musicService.filterByGenre(genre));
        if (StringUtils.isNotEmpty(title)) musics.addAll(musicService.filterByTitle(title));

        return ResponseEntity.status(HttpStatus.OK).body(musics);
    }
}
