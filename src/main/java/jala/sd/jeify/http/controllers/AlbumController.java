package jala.sd.jeify.http.controllers;

import jakarta.validation.Valid;
import jala.sd.jeify.domain.entities.Album;
import jala.sd.jeify.domain.entities.Music;
import jala.sd.jeify.http.dtos.AlbumRequestDTO;
import jala.sd.jeify.http.dtos.MusicRequestDTO;
import jala.sd.jeify.services.AlbumService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@RestController
@RequestMapping("/api/albums")
public class AlbumController {
    private final AlbumService albumService;
    
    @Autowired
    public AlbumController(AlbumService albumService) {
        this.albumService = albumService;
    }
    
    @GetMapping
    public ResponseEntity<Object> getAll() {
        return ResponseEntity.status(HttpStatus.OK).body(albumService.getAll());
    }
    
    @PostMapping
    public ResponseEntity<Album> save(@RequestBody @Valid AlbumRequestDTO data) {
        Album album = new Album();
        BeanUtils.copyProperties(data, album);
        return ResponseEntity.status(HttpStatus.OK).body(albumService.save(album));
    }

    @PutMapping("/{album_id}")
    public ResponseEntity<Album> updateMusic(@PathVariable UUID album_id, @RequestBody @Valid MusicRequestDTO data) {
        Album album = albumService.findById(album_id);
        BeanUtils.copyProperties(data, album);

        return ResponseEntity.status(HttpStatus.OK).body(albumService.update(album));
    }

    @PatchMapping("/{album_id}/{music_id}")
    public ResponseEntity<Object> addMusic(@PathVariable UUID album_id, @PathVariable @Valid UUID music_id) {
        albumService.addMusic(album_id, music_id);
        return ResponseEntity.status(HttpStatus.OK).body("Music added to album");
    }

    @DeleteMapping("/{album_id}")
    public ResponseEntity<Object> deleteMusic(@PathVariable UUID album_id) {
        albumService.delete(album_id);
        return ResponseEntity.status(HttpStatus.OK).body("Music " + album_id + " has been deleted.");
    }
}
