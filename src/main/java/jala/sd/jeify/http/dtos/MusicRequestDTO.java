package jala.sd.jeify.http.dtos;

public record MusicRequestDTO(String title, String genre, int duration, String artist) { }
