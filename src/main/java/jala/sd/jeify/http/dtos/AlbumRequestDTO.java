package jala.sd.jeify.http.dtos;

public record AlbumRequestDTO(String title) { }
