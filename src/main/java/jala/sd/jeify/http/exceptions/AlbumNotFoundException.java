package jala.sd.jeify.http.exceptions;

public class AlbumNotFoundException extends RuntimeException {
    public AlbumNotFoundException(String message) {
        super(message);
    }

    public AlbumNotFoundException() {
        super("Music not found.");
    }
}
