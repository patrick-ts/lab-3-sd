package jala.sd.jeify.http.exceptions;

public class MusicExistsInAlbumException extends RuntimeException {
    public MusicExistsInAlbumException() {
        super("Music Exists.");
    }

    public MusicExistsInAlbumException(String message) {
        super(message);
    }
}
