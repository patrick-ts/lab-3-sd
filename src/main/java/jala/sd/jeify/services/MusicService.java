package jala.sd.jeify.services;

import jala.sd.jeify.domain.entities.Music;
import jala.sd.jeify.domain.repositories.MusicRepository;
import jala.sd.jeify.http.exceptions.MusicNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
public class MusicService {
    final private MusicRepository repository;

    @Autowired
    public MusicService(MusicRepository repository) {
        this.repository = repository;
    }

    public List<Music> getAll() {
        return repository.findAll();
    }

    public Music save(Music music) {
        if(isThereMusic(music.getId())) {
            throw new RuntimeException("Music Founded. Not Possible To Save.");
        }

        return repository.save(music);
    }

    public Music update(Music music) {
        if(!isThereMusic(music.getId())) {
            throw new MusicNotFoundException("Music Not Found. Not Possible To Update.");
        }

        return repository.save(music);
    }

    public void delete(UUID id) {
        if(!isThereMusic(id)) {
            throw new MusicNotFoundException("Music Not Found. Not Possible To Delete.");
        }

        repository.deleteById(id);
    }

    public Music findById(UUID id) {
        if(!isThereMusic(id)) {
            throw new MusicNotFoundException();
        }

        return repository.getReferenceById(id);
    }

    public List<Music> filterByArtist(String artist) {
        return repository.filterByArtist(artist);
    }

    public List<Music> filterByGenre(String genre) {
        return repository.filterByGenre(genre);
    }

    public List<Music> filterByTitle(String title) {
        return repository.filterByTitle(title);
    }

    private Boolean isThereMusic(UUID id) {
        return repository.findById(id).isPresent();
    }
}
