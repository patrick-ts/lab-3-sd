package jala.sd.jeify.services;

import jala.sd.jeify.domain.entities.Album;
import jala.sd.jeify.domain.entities.Music;
import jala.sd.jeify.domain.repositories.AlbumRepository;
import jala.sd.jeify.domain.repositories.MusicRepository;
import jala.sd.jeify.http.exceptions.AlbumNotFoundException;
import jala.sd.jeify.http.exceptions.MusicExistsInAlbumException;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
public class AlbumService {
    private final AlbumRepository albumRepository;
    private final MusicService musicService;

    @Autowired
    public AlbumService(AlbumRepository albumRepository, MusicService musicService) {
        this.albumRepository = albumRepository;
        this.musicService = musicService;
    }

    public List<Album> getAll() {
        return albumRepository.findAll();
    }

    public Album save(Album album) {
        return albumRepository.save(album);
    }

    public Album update(Album album) {
        if (!isThereAlbum(album.getId())) {
            throw new AlbumNotFoundException("Album Not Found. Not Possible To Update.");
        }

        return albumRepository.save(album);
    }

    public void delete(UUID id) {
        if (!isThereAlbum(id)) {
            throw new AlbumNotFoundException("Album Not Found. Not Possible To Delete.");
        }

        albumRepository.deleteById(id);
    }

    public void addMusic(UUID albumId, UUID musicId) {
        if (!isThereAlbum(albumId)) {
            throw new AlbumNotFoundException();
        }

        Album album = albumRepository.getReferenceById(albumId);
        Music musicToAdd = musicService.findById(musicId);


        if(album.getMusicCollection().contains(musicToAdd)) {
            throw new MusicExistsInAlbumException();
        }

        album.getMusicCollection().add(musicToAdd);
        musicToAdd.setAlbum(album);

        musicService.save(musicToAdd);
        albumRepository.save(album);
    }

    public Album findById(UUID id) {
        if (!isThereAlbum(id)) {
            throw new AlbumNotFoundException();
        }

        return albumRepository.getReferenceById(id);
    }

    private Boolean isThereAlbum(UUID id) {
        return albumRepository.findById(id).isPresent();
    }
}
